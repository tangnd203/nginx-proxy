# Nginx-proxy 
![alt text](/images/image.png)
## A - Proxy
### 1 - What is proxy ?
- proxy (forward proxy) : ẩn đi danh tính của client với server bằng cách gửi các request thay cho client
### 2 - How does proxy work ?
- khi client tạo một request đến internet thông qua proxy server => proxy server sẽ đọc và phân tích request
- request sẽ được chuyển đến đúng server đích từ proxy server
- server nhận request và gửi response lại cho proxy server
- proxy server nhận dữ liệu, extracts và check xem có chứa malware không
- sau khi được đánh dấu an toàn => dẽ liệu sẽ chuyển đến cho client đã gửi request
### 3 - What can proxy do ?
a - Caching
- cache bất kỳ dữ liệu nào ít bị thay đổi, dữ liệu nào được truy cập thường xuyên => để giảm độ trễ , giảm traffic network, tăng băng thông mạng
![alt text](images/1.gif)

b - Anonymity
- server sẽ không biết thông tin về client gửi request đến từ đâu => server sẽ chỉ biết được địa chỉ của proxy server
![alt text](images/2.gif)

c - Traffic control
- kiểm soát tất cả traffic đi qua proxy server => có thể chặn một số nội dung không phù hợp (có thể chưa virus,...) được trả về, chặn các request đến các trang web độc hại, không an toàn,...

d - Logging
- tất cả các traffic đi qua proxy => đều được ghi lại vào logs và hiển thị thông qua logs
- được sử dụng để xác định bất kỳ mẫu nào hoạc đánh giá nhu cầu lưu vào cache cho một số trang web nhất định
![alt text](images/3.gif)

## B - Reverse Proxy
### 1 - What is reverse proxy ?
- reverse proxy : ẩn danh final server phục vụ các request từ client
### 2 - How does reverse proxy work ?
- client gửi request qua reverse proxy server
- reverse proxy server kiểm tra và xác định backend server nào xử lý request đó dựa trên nhiều tác nhân (thuật toán load balancing, URL patterns hay server health check,...) => chuyển request đến backend server được chọn
- backend server sẽ xử lý request và gửi response lại cho reverse proxy 
- reverse proxy nhận được response từ backend server và gửi lại cho client đã request
### 3 - What can reverse proxy do ?
a - caching
![alt text](images/4.gif)
b - Anonymity
![alt text](images/5.gif)
c - Load balancing
![alt text](images/6.gif)
d - Experimentation
- khi triển khai tính năng mới => triển khai theo canary fashion (90% traffic được chuyển sang cho version A và 10% traffic chuyển sang cho version B)

e - Router/Ingress
- thực hiện như một cổng vào hoạc bộ định tuyến trong kiến trúc của kubernetes hoặc microsevice 
## C - Setup reverse proxy (Nginx)
### 1 - Mô hình
![alt text](images/image1.png)
- reverse proxy : 192.168.188.132
- webserver 1 : 192.168.188.134
- webserver 2 : 192.168.188.135
### 2 - Install Nginx trên 3 máy
```
    $ sudo apt update
    $ sudo apt install nginx -y
```
### 3 - Configure reverse proxy
a - Case 1
- config file /etc/nginx/sites-available/default
```
    $ sudo vim /etc/nginx/sites-available/default
```
```bash
    server {
        listen 80;
        server_name adc.vmware.lab;
    
        proxy_set_header HOST $host;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    
        location / {
            proxy_pass http://backend;
        }
    }
```
- Giải thích
  + listen 80 : Nginx lắng nghe các kết nối trên port 80
  + server_name : chỉ thị server name hoặc các giá trị mà Nginx sẽ sử dụng để phân biệt các virtual host khi nhiều virtual host (vhost) cùng lắng nghe trên cùng một địa chỉ IP và cổng
  + proxy_set_header : chỉ thị set headers cho requests được forward đến backend server
    * Host : chứa original host được client request ( domain name + port )  => được Nginx giữ trong biến $host
    * X-Forwarded-Proto : chứa protocol được sử dụng bởi người dùng ban đầu connect (HTTP, HTTPS) => Nginx giữ trong biến $scheme
    * X-Real-IP : chứa ip address duy nhất thuộc về remote client => Nginx giữ trong biến $remote_addr
    * X-Forwarded-For : chứa ip address của client đã gửi original request , có thể chứa list ip address ( ip client xuất hiện trước , sau đó đến tất cả các ip address của reverse proxy server đã forward through ) => Nginx giữ chúng trong biến $proxy_add_x_forwarded_for
  + location / { proxy_pass http://backend; } : block chỉ định location và instructs Nginx proxy_pass (forward) request đến backend server đã được định nghĩa ở upstream 
- config file /etc/nginx/nginx.conf
```
    $ sudo vim /etc/nginx/nginx.conf
```
```bash
    http {
        …
        upstream backend {
            server 192.168.188.134;
            server 192.168.188.135;
        }
    }
```
b - Case 2
- Tạo 2 file config và set server name là wss1.com và wss2.com
  + wss1.conf
  ```
    $ sudo vim /etc/nginx/conf.d/wss1.conf
  ```
  ```bash
    server {
        listen 80;
        server_name wss1.com;
 
        proxy_set_header HOST $host;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
 
        location / {
                proxy_pass http://192.168.188.134;
        }
    }
  ```
  + wss2.conf
  ```
    $ sudo vim /etc/nginx/conf.d/wss2.conf
  ```
  ```bash
    server {
        listen 80;
        server_name wss2.com;
 
        proxy_set_header HOST $host;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
 
        location / {
                proxy_pass http://192.168.188.135;
        }
    }
  ```
- Add host domain wss1.com và wss2.com về ip reverse proxy
```
    $ sudo vim /etc/hosts
```
```bash
    192.168.188.134 wss1.com
    192.168.188.135 wss2.com
    192.168.188.100 adc.vmware.lab
```
### 4 - Restart Nginx và Test 
```
    $ sudo service nginx restart
    $ sudo service nginx status
```
- request đến wss1.com
```
    $ curl -i http://wss1.com
```
![alt text](images/img1.png)

- request đến wss2.com
```
    $ curl -i http://wss2.com
```
![alt text](images/img2.png)

- request đến abc.vmware.lab
```
    $ curl -i http://adc.vmware.lab
```
![alt text](images/img3.png)
![alt text](images/img4.png)

